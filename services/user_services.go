package services

import (
	"gitlab.com/nazirait/mvc/domain"
	"gitlab.com/nazirait/mvc/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)

}
